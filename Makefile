TARGET = somedb

.PHONY: build
build:
	go build -o $(TARGET) main/main.go

.PHONY: clean
clean:
	rm -rf $(TARGET)
