package main

import (
	"bufio"
	"fmt"
	"os"
	"somedb/sql"
)

func prompt() {
	fmt.Printf("somedb > ")
}

// Read command from stdin and return the command without '\n'
func readCommand() string {
	input := bufio.NewReader(os.Stdin)
	text, err := input.ReadString('\n')
	if err != nil {
		panic(err)
	}
	return text[:len(text)-1]
}

func doMetaCommand(input string) {
	if input == ".exit" {
		os.Exit(0)
	} else {
		fmt.Printf("Unknown meta command %s\n", input)
	}
}

func doStatement(input string) {
	sql.DoStatement(input)
}

func loop() {
	for {
		prompt()
		input := readCommand()
		if len(input) == 0 {
			continue
		} else if input[0] == '.' {
			doMetaCommand(input)
		} else {
			doStatement(input)
		}
	}
}

func main() {
	fmt.Println("Welcome to SomeDB!")
	loop()
}
