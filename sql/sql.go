package sql

import (
	"errors"
	"fmt"
	"strings"
)

type Statement interface {
	Exec()
}

type SelectStmt struct {
}

func (*SelectStmt) Exec() {
	fmt.Println("This is a SELECT statement.")
}

type InsertStmt struct {
}

func (*InsertStmt) Exec() {
	fmt.Println("This is an INSERT statement.")
}

func parseStatement(input string) (Statement, error) {
	keyword := strings.Split(input, " ")[0]
	switch keyword {
	case "SELECT":
		return &SelectStmt{}, nil
	case "INSERT":
		return &InsertStmt{}, nil
	default:
		errMsg := fmt.Sprintf("Unknown keyword %s\n", keyword)
		return nil, errors.New(errMsg)
	}
}

func execStatement(statement Statement) {
	statement.Exec()
}

func DoStatement(input string) {
	stmt, err := parseStatement(input)
	if err != nil {
		fmt.Print(err)
		return
	}
	execStatement(stmt)
}
